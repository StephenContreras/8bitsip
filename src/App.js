import React from 'react';
import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Good things are coming but for now we are <br/>
          <code>Loading...</code>
        </p>
        <a
          className="App-link"
          href="https://twitter.com/8bitsip"
          target="_blank"
          rel="noopener noreferrer"
        >Twitter</a> 
        <a
          className="App-link"
          href="https://instagram.com/8bitsip"
          target="_blank"
          rel="noopener noreferrer"
        >Instagram</a> 
      </header>
    </div>
  );
}

export default App;
